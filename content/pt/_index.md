---
title: "Crypto Friday"

description: "Everything about cybersecurity & privacy."
cascade:
  featured_image: '/cryptofriday.bkg.png'

omit_header_text : true
---

### Nosso próximo evento

Sexta-feira, 26/05/2023, 19:00

[SEBRAE-SC](https://www.google.com/maps/place/Sebrae+Santa+Catarina/@-27.5716413,-48.5144715,17z/data=!3m1!4b1!4m6!3m5!1s0x9527388e9bbaf475:0x6f4cfe6b9403e89b!8m2!3d-27.5716413!4d-48.5118912!16s%2Fg%2F11b7gpjb52)

Inscrições gratuitas: [Sympla](https://www.sympla.com.br/evento/cryptofriday-meetup-de-ciberseguranca/1969817)

### Nós somos a Crypto Friday ...

... e nós nos reunimos em alguma ou outra sexta-feira à noite para falarmos de segurança, privacidade, direito digital, criptografia, criptomoedas, web3, e qualquer outro assunto que envolva esse emaranhado com pessoas e computadores.

[![Nosso grupo no Telegram](telegram_logo.svg)](https://t.me/the_cryptofriday)

Telegram: [`https://t.me/the_cryptofriday`](https://t.me/the_cryptofriday)

### Quer trazer uma atividade para a gente?

Preencha nossa [Call for Papers](https://docs.google.com/forms/d/e/1FAIpQLSeKgnhrvvlyDaHri-bcR9lEAMYtfxCXxjmH91nbHoHTe47Jcw/viewform)!

### Quer falar com a gente?

`oi arroba crypto friday ponto space` `;)`

[![Logo](cryptofriday.svg)](https://t.me/the_cryptofriday)
